## Projet

Problématique: La fidélisation des clients en salle de sport.

Proposition: Une application pour mettre en relation les membres d'une salle, en se basant sur les horaires et les objectifs défini par le membre lors de son inscription. 
Pour cela les membres du club complèteront les informations clé suivante, et pourront choisir d'être mis en relation:

- Age.
- Genre.
- Horaires d'entrainement.
- Objectif / Programme qu'il souhaites suivre.

### Avancement:

🚧 - WIP
✅ - Done


## Fonctionnalités principales

### Back-end

- S'enregistrer ✅
  https://gitlab.com/jldjld/tym/-/blob/master/src/Controller/User/Auth/RegistrationController.php
  - Compléter un formulaire pour être mis en relation avec d'autres membres de la salle. ✅
  https://gitlab.com/jldjld/tym/-/blob/master/src/Form/RegistrationFormType.php
- Se connecter ✅
  https://gitlab.com/jldjld/tym/-/blob/master/src/Controller/User/Auth/LoginController.php
- Consulter son profil ✅
  - Consulter la liste des membres compatibles. ✅
    https://gitlab.com/jldjld/tym/-/blob/master/src/Controller/User/Find/FindUserController.php
- Consulter le profil d'un membre. ✅
- Inviter un membre à faire une séance. ✅

#### Implémenter les endpoints suivants
  ##### /api/v1/register
  - S'enregistrer ✅
     - Compléter un formulaire pour être mis en relation avec d'autres membres de la salle.
     - Test PHPUnit ✅
  ##### /api/v1/login
  - Se connecter ( LexikAuthenticatorBundle ) ✅
  - Test PHPUnit ✅
  ##### /api/v1/rofile/:id
  - Consulter le profil d'un membre ✅
  ##### /api/v1/profile/
  - Consulter son profil ✅
    - Test PHPUnit ✅
  - Consulter la liste des membres compatibles. ✅

  ##### /api/v1/invite/:id
  - Créer une invitation ✅
    - Test PHPUnit ✅
  - Accepter une invitation ✅
    - Test PHPUnit 🚧
  - Refuser une invitation ✅
    - Test PHPUnit 🚧


  ##### Implémenter API Platform 🚧

### Front-end

- S'enregistrer 
  - Compléter un formulaire pour être mis en relation avec d'autres membres de la salle.
- Se connecter 
- Consulter la liste des membres compatibles.
- Consulter le profil d'un membre.
- Inviter un membre à faire une séance.
