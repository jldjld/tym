<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240107104314 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE availability (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', days LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json)\', time_slot VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE availability_user (availability_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', user_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', INDEX IDX_3D8C2AB061778466 (availability_id), INDEX IDX_3D8C2AB0A76ED395 (user_id), PRIMARY KEY(availability_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE availability_gym (availability_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', gym_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', INDEX IDX_B043009761778466 (availability_id), INDEX IDX_B0430097BD2F03 (gym_id), PRIMARY KEY(availability_id, gym_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE availability_user ADD CONSTRAINT FK_3D8C2AB061778466 FOREIGN KEY (availability_id) REFERENCES availability (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE availability_user ADD CONSTRAINT FK_3D8C2AB0A76ED395 FOREIGN KEY (user_id) REFERENCES `user` (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE availability_gym ADD CONSTRAINT FK_B043009761778466 FOREIGN KEY (availability_id) REFERENCES availability (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE availability_gym ADD CONSTRAINT FK_B0430097BD2F03 FOREIGN KEY (gym_id) REFERENCES gym (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE availability_user DROP FOREIGN KEY FK_3D8C2AB061778466');
        $this->addSql('ALTER TABLE availability_user DROP FOREIGN KEY FK_3D8C2AB0A76ED395');
        $this->addSql('ALTER TABLE availability_gym DROP FOREIGN KEY FK_B043009761778466');
        $this->addSql('ALTER TABLE availability_gym DROP FOREIGN KEY FK_B0430097BD2F03');
        $this->addSql('DROP TABLE availability');
        $this->addSql('DROP TABLE availability_user');
        $this->addSql('DROP TABLE availability_gym');
    }
}
