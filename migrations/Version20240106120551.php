<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240106120551 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE gym CHANGE opening_hours opening_hours LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json)\'');
        $this->addSql('ALTER TABLE user ADD is_first_login TINYINT(1) NOT NULL, CHANGE goals goals LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\'');
        $this->addSql('ALTER TABLE user_invitation CHANGE accepted accepted LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json)\', CHANGE declined declined LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json)\', CHANGE pending pending LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json)\'');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE `user` DROP is_first_login, CHANGE goals goals LONGTEXT NOT NULL COMMENT \'(DC2Type:simple_array)\'');
        $this->addSql('ALTER TABLE gym CHANGE opening_hours opening_hours LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:simple_array)\'');
        $this->addSql('ALTER TABLE user_invitation CHANGE accepted accepted LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:simple_array)\', CHANGE declined declined LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:simple_array)\', CHANGE pending pending LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:simple_array)\'');
    }
}
