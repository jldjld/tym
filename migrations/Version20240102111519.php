<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240102111519 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE club (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', name VARCHAR(255) NOT NULL, description VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE gym (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', owner_id BINARY(16) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', club_id BINARY(16) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', name VARCHAR(255) NOT NULL, description VARCHAR(255) DEFAULT NULL, opening_hours LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:simple_array)\', phone VARCHAR(20) DEFAULT NULL, city VARCHAR(255) NOT NULL, INDEX IDX_7F27DBED7E3C61F9 (owner_id), INDEX IDX_7F27DBED61190A32 (club_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `user` (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', main_gym_id BINARY(16) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', club_id BINARY(16) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', email VARCHAR(180) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', password VARCHAR(255) NOT NULL, birthdate DATE DEFAULT NULL, gender VARCHAR(1) DEFAULT NULL, goals LONGTEXT NOT NULL COMMENT \'(DC2Type:simple_array)\', availability VARCHAR(30) DEFAULT NULL, city VARCHAR(255) NOT NULL, username VARCHAR(55) NOT NULL, is_verified TINYINT(1) NOT NULL, avatar VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), INDEX IDX_8D93D6499FA19715 (main_gym_id), INDEX IDX_8D93D64961190A32 (club_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_invitation (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', user_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', accepted LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:simple_array)\', declined LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:simple_array)\', pending LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:simple_array)\', INDEX IDX_567AA74EA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE gym ADD CONSTRAINT FK_7F27DBED7E3C61F9 FOREIGN KEY (owner_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE gym ADD CONSTRAINT FK_7F27DBED61190A32 FOREIGN KEY (club_id) REFERENCES club (id)');
        $this->addSql('ALTER TABLE `user` ADD CONSTRAINT FK_8D93D6499FA19715 FOREIGN KEY (main_gym_id) REFERENCES gym (id)');
        $this->addSql('ALTER TABLE `user` ADD CONSTRAINT FK_8D93D64961190A32 FOREIGN KEY (club_id) REFERENCES club (id)');
        $this->addSql('ALTER TABLE user_invitation ADD CONSTRAINT FK_567AA74EA76ED395 FOREIGN KEY (user_id) REFERENCES `user` (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE gym DROP FOREIGN KEY FK_7F27DBED7E3C61F9');
        $this->addSql('ALTER TABLE gym DROP FOREIGN KEY FK_7F27DBED61190A32');
        $this->addSql('ALTER TABLE `user` DROP FOREIGN KEY FK_8D93D6499FA19715');
        $this->addSql('ALTER TABLE `user` DROP FOREIGN KEY FK_8D93D64961190A32');
        $this->addSql('ALTER TABLE user_invitation DROP FOREIGN KEY FK_567AA74EA76ED395');
        $this->addSql('DROP TABLE club');
        $this->addSql('DROP TABLE gym');
        $this->addSql('DROP TABLE `user`');
        $this->addSql('DROP TABLE user_invitation');
    }
}
