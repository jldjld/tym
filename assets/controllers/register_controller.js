import { Controller } from '@hotwired/stimulus';
import axios from "axios";

export default class extends Controller {
    connect() {
        let city     = document.querySelector('#registration_form_city');
        let dataList = document.getElementById('json-cities');
        let goals    = document.querySelector('#registration_form_goals');

        city.addEventListener('input', (event) => {
            // Start searching when 3 characters are typed
            if ( 3 <= city.value.length ) {
                Promise.all([
                    /**
                     * Using government cities list API
                     * https://geo.api.gouv.fr/decoupage-administratif/communes#advanced
                     */
                    axios.get('https://geo.api.gouv.fr/communes?nom=' + city.value + '&fields=code,nom,departement' )
                ])
                .then(response => {
                    let cities = response[0].data;

                    cities.forEach(element => {
                        let option = document.createElement('option');
                        // Set the value using the item in the JSON array.
                        option.value     = element.nom;
                        let optClassName = element.nom.replace(/\s|'|\(|\)/g, '');

                        option.classList.add( 'city-' + optClassName );
                        // Add city to datalist if empty or city does not exists in list
                        if ( 0 === dataList.childNodes.length || ! dataList.querySelector( '.city-' + optClassName ) ) {
                            // Add the <option> element to the <datalist>.
                            dataList.appendChild(option);
                        }
                    });

                    
                    event.preventDefault();
                })
                .then(response => {
                   city.addEventListener('change',function(){
                        // empty datalist if empty city input
                        if(city === null) {
                            dataList.innerHTML = "";
                        }
                        let registerDetails = document.querySelector('.register_details');
                        /**
                         * Delete register form fields on city change
                         */
                        if ( registerDetails !== null ) {
                            registerDetails.innerHTML = "";
                        }
                    })
                })
                .catch(error => {
                    console.log(error);
                });

                    
            }
        });

        // Avoid selection of more than two goals
        if( goals ) {
            goals.addEventListener('input', (event) => {
                if (event.target.type === 'checkbox') {
                    const checked = document.querySelectorAll('input[type="checkbox"]:checked')
                    
                    if( checked.length == 2) {
                        let unChecked = document.querySelectorAll('input[type="checkbox"]:not(:checked)');
                        
                        unChecked.forEach(element => {
                            element.disabled = 'disabled';
                        })
                    } else {
                        let checkboxes = document.querySelectorAll('input[type="checkbox"]');
                        checkboxes.forEach( element => {
                            element.disabled = undefined
                          })
                    }
                  }
            });
        }
    }
}
