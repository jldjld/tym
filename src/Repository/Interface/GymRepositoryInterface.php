<?php

namespace App\Repository\Interface;

use App\Entity\Gym;

interface GymRepositoryInterface
{
    public function findByCity(string $city): ?array;

    public function save(Gym $entity, bool $flush = false): void;

    public function remove(Gym $entity, bool $flush = false): void;
}
