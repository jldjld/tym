<?php

namespace App\Repository\Interface;

use App\Entity\User;
use Symfony\Component\Uid\Uuid;

interface UserRepositoryInterface
{
    /**
     * @return User[] Returns an array of User objects
     */
    public function findByCityAndClub(string $city, Uuid $club_id): array;

    public function findOneBy(array $criteria, array $order_by = null): ?User;

    public function find($id, $lockMode = null, $lockVersion = null): ?User;

    public function save(User $entity, bool $flush = false): void;

    public function remove(User $entity, bool $flush = false): void;
}
