<?php

namespace App\Repository\Interface;

use App\Entity\Club;

interface ClubRepositoryInterface
{
    /**
     * @return Club[] Returns an array of Club objects
     */
    public function findAll(): array;

    public function save(Club $entity, bool $flush = false): void;

    public function remove(Club $entity, bool $flush = false): void;
}
