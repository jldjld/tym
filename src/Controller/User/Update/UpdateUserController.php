<?php

namespace App\Controller\User\Update;

use App\Entity\User;
use App\Form\RegistrationFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\CurrentUser;

#[Route("/api/v1/profile")]
class UpdateUserController extends AbstractController
{
    public function __construct(
    ) {
    }

    /**
     * Update user data
     *
     * @param User $user
     * @param  Request $request
     * @param  EntityManagerInterface $entityManager
     * @return JsonResponse
     */
    #[Route('/update', name: 'api_profile_update', methods:["GET","PATCH"])]
    public function updateUserData(
        #[CurrentUser] User    $user,
        Request                $request,
        EntityManagerInterface $entityManager
    ): JsonResponse {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        if($user->isVerified() !== true) {
            return $this->json(
                'Member account is not verified.',
                Response::HTTP_FORBIDDEN,
            );
        }

        // Using label to pass current route name to the form
        $form = $this->createForm(RegistrationFormType::class, $user, ['label' => 'route_api_profile_update' ]);
        $form->handleRequest($request);
        $form->submit(
            json_decode(
                $request->getContent(),
                true
            ),
            false
        );

        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager->persist($user);
            $entityManager->flush();

            $responseData = [];

            // Check if getters are not empty and include them in the response
            if ($user->getGender() !== null) {
                $responseData['gender'] = $user->getGender();
            }
            if ($user->getBirthdate() !== null) {
                $responseData['birthdate'] = $user->getBirthdate();
            }
            if ($user->getGoals() !== null) {
                $responseData['goals'] = $user->getGoals();
            }
            if ($user->getAvailabilities() !== null) {
                $responseData['availabilities'] = $user->getAvailabilities();
            }
            if ($user->getMainGym() !== null) {
                $responseData['mainGym'] = $user->getMainGym();
            }

            // Groups is needed in order to avoid circular reference error
            return $this->json(
                $responseData,
                Response::HTTP_OK,
                [],
                ['groups' => ['user', 'gym']]
            );
        }

        return $this->json(
            $form->getErrors(true),
            Response::HTTP_BAD_REQUEST
        );

    }
}
