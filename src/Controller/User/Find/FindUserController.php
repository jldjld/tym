<?php

namespace App\Controller\User\Find;

use App\Entity\User;
use App\Controller\User\Helpers\UserHelper;
use App\Repository\Interface\UserRepositoryInterface;
use App\Service\UserMatcher\UserMatcher;
use DateTimeInterface;
use InvalidArgumentException;
use Psr\Cache\CacheItemInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\UserNotFoundException as ExceptionUserNotFoundException;
use Symfony\Component\Uid\Uuid;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route("/api/v1/profile")]
class FindUserController extends AbstractController
{
    public function __construct(
        private readonly UserRepositoryInterface $userRepositoryInterface,
        private readonly UserMatcher    $userMatcher,
        private TranslatorInterface     $translator,
        private readonly CacheInterface $cache
    ) {
    }

    /**
     * Get Club users from the same city
     *
     * @param  string      $city
     * @param  Uuid        $club_id
     * @return User[]|null
     */
    public function findByCityAndClub(string $city, Uuid $club_id): ?array
    {
        return $this->userRepositoryInterface->findByCityAndClub($city, $club_id);
    }

    /**
     * Get current user profile
     *
     * @return Response
     * @throws \Psr\Cache\InvalidArgumentException
     */
    #[Route('/', name: 'api_profile', methods:["GET"])]
    public function index(): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $user = $this->getUser();
        if (!$user instanceof User) {
            throw new ExceptionUserNotFoundException('Should not happen, user does not exist.');
        }

        /**
         * Display user profile information alone
         * without matched users
         * if account has not been verified
        */
        if (true !== $user->isVerified()) {
            //$message =  $this->translator->trans('Merci de consulter vos emails pour vérifier votre compte, afin accéder à votre profil');
            //$this->addFlash('notice', $message);
            return $this->json(
                [
                    $user->getId(),
                    [
                        'is_verified' => $user->isVerified()
                    ],
                ],
                Response::HTTP_UNAUTHORIZED,
                [],
                ['groups' => ['user', 'gym']]
            );
        }

        // Users data cannot be empty has they are needed on registration step
        $birthDate = $user->getBirthdate();
        if (empty($birthDate) || !$birthDate instanceof DateTimeInterface) {
            throw new InvalidArgumentException('Birthdate should not be null, happened on user id:' . $user->getId());
        }


        $age          = UserHelper::getUserAge($birthDate);
        $goals        = $user->getGoals();
        $availability = $user->getAvailabilities();
        $user_data    = [
            'username'     => $user->getUsername(),
            'age'          => $age,
            'goals'        => $goals,
            'availability' => $availability,
            'avatar'       => $user->getAvatar(),
        ];


        /**
         * Verify if cache exists for user matchedMembers
         * get matchedMembers if expired
         */
        $key             = sprintf('user_%d_%s', $user->getId(), date("Ymd"));
        $matchedMembers  = $this->cache->get(
            $key,
            function (
                CacheItemInterface $item
            ) use (
                $user,
                $birthDate,
                $goals,
                $availability
            ) {
                // Renew matched results after one day
                $item->expiresAfter(strtotime('+ 1 days'));
                /**
                 * Get Gym members or club members from the area
                 * if no gym members exists yet
                 */
                return $this->userMatcher->getMatchedMembers($user, $birthDate, $goals, $availability);
            }
        );

        if(empty($matchedMembers)) {
            $matchedMembers = $this->userMatcher->getMatchedMembers($user, $birthDate, $goals, $availability);
        }

        return $this->json(
            [
                $user_data,
                [
                    'is_verified' => $user->isVerified()
                ],
                $matchedMembers
            ],
            Response::HTTP_FOUND,
            [],
            ['groups' => ['user', 'gym']]
        );
    }

    /**
     * Return single member data
     *
     * @param  Uuid $id
     * @return null|JsonResponse
     */
    #[Route('/{id}', name: 'api_profile_id', methods:["GET"])]
    public function findMemberProfile(Uuid $id): ?JsonResponse
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $member = $this->userRepositoryInterface->findOneBy(['id' => $id]);
        if (empty($member)) {
            throw new ExceptionUserNotFoundException('Should not happen, user does not exist for id :' . $id);
        }

        return $this->json(
            $member,
            Response::HTTP_FOUND,
            [],
            ['groups' => ['user', 'gym']]
        );
    }
}
