<?php

namespace App\Controller\User\Auth;

use App\Controller\User\Notification\UserRegisterNotifications;
use App\Entity\User;
use App\Form\RegistrationFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Asset\Package;
use Symfony\Component\Asset\VersionStrategy\EmptyVersionStrategy;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use SymfonyCasts\Bundle\VerifyEmail\VerifyEmailHelperInterface;

/**
 * Controller class for registration.
 *
 * @package App\Controller\User\Auth
 * @category Controller
 * @license  https://opensource.org/licenses/MIT MIT License
 * @link     https://example.com
 */
#[Route("/api/v1")]
class RegistrationController extends AbstractController
{
    public function __construct(
        private readonly VerifyEmailHelperInterface $verifyEmailHelper,
        private readonly TranslatorInterface        $translator,
        private readonly UserRegisterNotifications $notifications
    ) {
    }

    /**
     * Adding new User
     *
     * @param Request $request
     * @param UserPasswordHasherInterface $userPasswordHasher
     * @param EntityManagerInterface $entityManager
     * @return Response
     * @throws TransportExceptionInterface
     */
    #[Route(
        '/register',
        name: 'api_register',
        defaults: [
            '_api_resource_class' => User::class,
        ],
    )]
    public function register(
        Request                     $request,
        UserPasswordHasherInterface $userPasswordHasher,
        EntityManagerInterface      $entityManager
    ): Response {
        // Redirect to profile if user is already connected
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw new AccessDeniedHttpException('User is already logged in.');
        }

        $user = new User();
        // [ 'csrf_protection' => true ]
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);
        $form->submit(
            json_decode(
                $request->getContent(),
                true
            ),
            false
        );

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setRoles(['ROLE_USER']);
            // encode the plain password
            $user->setPassword(
                $userPasswordHasher->hashPassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );

            $package = new Package(new EmptyVersionStrategy());
            $defaultAvatar = $package->getUrl('build/images/avatar.png');
            // Set Default avatar
            $user->setAvatar($defaultAvatar);

            $entityManager->persist($user);
            $entityManager->flush();

            $user_id = $user->getId();

            $signatureComponents = $this->verifyEmailHelper->generateSignature(
                'api_verify_email',
                $user_id,
                $user->getEmail(),
                ['id' => $user_id]
            );

            $this->notifications->sendValidationEmail($user, $user_id, $signatureComponents);

            $message = $this->translator->trans('Un email de validation vous a été envoyé');
            //$this->addFlash('success', $message);

            // Groups is needed in order to avoid circular reference error
            return $this->json(
                ['user_id' => $user_id],
                Response::HTTP_CREATED,
                [],
                ['groups' => ['user', 'gym']]
            );
        }

        return $this->json(
            $form->getErrors(true),
            Response::HTTP_BAD_REQUEST
        );
    }
}
