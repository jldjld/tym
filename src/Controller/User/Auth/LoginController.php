<?php

namespace App\Controller\User\Auth;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\CurrentUser;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * Controller class for login.
 *
 * @package App\Controller\User\Auth
 * @category Controller
 * @license  https://opensource.org/licenses/MIT MIT License
 * @link     https://example.com
 */
#[Route("/api/v1")]
class LoginController extends AbstractController
{
    /**
     * @param AuthenticationUtils $authenticationUtils
     * @param User|null           $user
     * @return Response
     */
    #[Route('/login', name: 'api_login')]
    public function index(AuthenticationUtils $authenticationUtils, #[CurrentUser]User $user): Response
    {
        // Redirect to profile if user is connected
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->redirectToRoute('api_profile');
        }

        if (null == $user) {
            return $this->json(
                [
                'message' => 'missing credentials',
                ],
                Response::HTTP_UNAUTHORIZED
            );
        }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        // $lastUsername = $authenticationUtils->getLastUsername();
        return $this->json(
            [
            'user'  => $user->getUserIdentifier(),
            'error' => $error
            ]
        );
    }
}
