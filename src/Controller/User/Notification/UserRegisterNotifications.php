<?php

namespace App\Controller\User\Notification;

use App\Entity\User;
use App\Repository\Interface\UserRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use InvalidArgumentException;
use Psr\Log\LoggerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\UserNotFoundException as ExceptionUserNotFoundException;
use Symfony\Component\Uid\Uuid;
use Symfony\Contracts\Translation\TranslatorInterface;
use SymfonyCasts\Bundle\VerifyEmail\Exception\VerifyEmailExceptionInterface;
use SymfonyCasts\Bundle\VerifyEmail\Model\VerifyEmailSignatureComponents;
use SymfonyCasts\Bundle\VerifyEmail\VerifyEmailHelperInterface;

/**
 * Controller class for registration notifications.
 *
 * @package App\Controller\User\Auth
 * @category Controller
 * @license  https://opensource.org/licenses/MIT MIT License
 * @link     https://example.com
 */
#[Route("/api/v1")]
class UserRegisterNotifications extends AbstractController
{
    public function __construct(
        private readonly VerifyEmailHelperInterface $verifyEmailHelper,
        private readonly LoggerInterface            $logger,
        private readonly MailerInterface            $mailer,
        private readonly TranslatorInterface        $translator,
        private readonly UserRepositoryInterface $userRepositoryInterface
    ) {
    }

    /**
     * Verify user email address
     *
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return JsonResponse
     * @throws Exception|TransportExceptionInterface
     */
    #[Route("/verify", name:"api_verify_email", methods:["GET"])]
    public function verifyUserEmail(
        Request $request,
        EntityManagerInterface $entityManager
    ): JsonResponse {
        // retrieve the user id from the url
        $id = $request->get('id');

        // Verify the user id exists and is not null
        if (null == $id) {
            throw new InvalidArgumentException('Should not happen, user id is null');
        }

        $user = $this->userRepositoryInterface->find($id);
        // Ensure the user exists in persistence
        if (!$user instanceof User) {
            throw new ExceptionUserNotFoundException('Should not happen, user does not exist');
        }

        // Do not get the User's Id or Email Address from the Request object
        try {
            $this->verifyEmailHelper->validateEmailConfirmation($request->getUri(), (string) $user->getId(), (string) $user->getEmail());
        } catch (VerifyEmailExceptionInterface $e) {
            //$this->addFlash('verify_email_error', $e->getReason());
            $this->logger->error('Email validation failed: ' . $e->getMessage());

            throw new Exception('Email validation failed: ' . $e->getMessage());
        }

        // Mark your user as verified. e.g. switch a User::verified property to true
        $user->setIsVerified(true);

        $message = $this->translator->trans('Email adress as been successfully verified.');
        //$this->addFlash('success', $message);

        $entityManager->flush();

        // Send user confirmation email
        $this->sendConfirmationEmail($user, $id);

        return $this->json(
            ['message' => $message],
            Response::HTTP_OK
        );
    }


    /**
     * Send email verification
     *
     * @param User $user
     * @param Uuid $user_id
     * @param VerifyEmailSignatureComponents $signatureComponents
     * @return void
     * @throws TransportExceptionInterface|Exception
     */
    public function sendValidationEmail(User $user, Uuid $user_id, VerifyEmailSignatureComponents $signatureComponents): void
    {
        $subject = getenv('PROJECT_NAME') . ' - Validation de votre adresse email';
        $email   = $user->getEmail();
        if(empty($email) || !is_string($email)) {
            $this->logger->error('Email is empty on sendVerificationEmail for id: ' . $user_id);
            throw new Exception('Email is empty on sendVerificationEmail for id: ' . $user_id);
        }

        $email = new TemplatedEmail();
        $email->to();
        $email->subject($subject);
        $email->htmlTemplate('registration/_validation_email.html.twig');
        $email->context(
            [
                'app_name'        => getenv('PROJECT_NAME'),
                'sender_email'    => getenv('ADMIN_EMAIL'),
                'signed_url'      => $signatureComponents->getSignedUrl(),
                'username'        => $user->getUsername(),
                'expiration_date' => new \DateTime('+3 days'),
            ]
        );

        $this->mailer->send($email);
    }

    /**
     * Send email confirmation
     *
     * @param User $user
     * @param Uuid $user_id
     * @return void
     * @throws TransportExceptionInterface|Exception
     */
    public function sendConfirmationEmail(User $user, Uuid $user_id): void
    {
        $username  = $user->getUsername();
        if(empty($username) || !is_string($username)) {
            $this->logger->error('Username is empty on sendConfirmationEmail for id: ' . $user_id);
            throw new Exception('Email is empty on sendVerificationEmail for id: ' . $user_id);
        }

        $subject   = 'Vous y êtes' . $username . ' !';
        $login_url = getenv('API_URL') . '/login';

        $email = new TemplatedEmail();
        $email->to($user->getEmail());
        $email->subject($subject);
        $email->htmlTemplate('registration/_confirmation_email.html.twig');
        $email->context(
            [
                'app_name'     => getenv('PROJECT_NAME'),
                'sender_email' => getenv('ADMIN_EMAIL'),
                'username'     => $username,
                'login_url'    => $login_url,
            ]
        );

        $this->mailer->send($email);
    }
}
