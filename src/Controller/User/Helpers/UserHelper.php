<?php

namespace App\Controller\User\Helpers;

use App\Entity\User;
use App\Repository\UserRepository;
use DateInterval;
use DateTimeInterface;
use Symfony\Component\Security\Core\Exception\UserNotFoundException;
use Symfony\Component\Uid\Uuid;

class UserHelper
{
    /**
     * Get Verified Users
     *
     * @param  User[] $users
     * @param string $role
     * @return array|User[]
     */
    public static function getVerifiedUsers(array $users, string $role): array
    {
        $verifiedUsers = [];
        foreach ($users as $user) {
            if (true !== $user->isVerified()) {
                continue;
            }

            if (!in_array($role, $user->getRoles())) {
                continue;
            }

            $verifiedUsers[] = $user;
        }

        return $verifiedUsers;
    }


    /**
     * Display member age
     *
     * @param DateTimeInterface $birthDate
     * @return DateInterval|false
     */
    public static function getUserAge(DateTimeInterface $birthDate): DateInterval|false
    {
        $today = date("Y");
        $diff  = date_diff($birthDate, date_create($today));

        return $diff;
    }


    /**
     * Get connected user id
     *
     * @return Uuid|UserNotFoundException
     */
    public function getCurrentUserId(User $user): Uuid|UserNotFoundException
    {
        if (null === $user || !$user instanceof User || !$user->getId() instanceof Uuid) {
            throw new UserNotFoundException();
        }

        return $user->getId();
    }


    /**
     * @param  Uuid $userId
     * @return User|UserNotFoundException
     */
    public function userExists(UserRepository $userRepository, Uuid $userId): User|UserNotFoundException
    {
        // Verify users exists
        $user = $userRepository->findOneBy(['id' => $userId]);
        // @todo use createnotfound exception with throw
        if (empty($user)) {
            throw new UserNotFoundException();
        }

        return $user;
    }

}
