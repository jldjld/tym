<?php

namespace App\Controller\UserInvitation\Decline;

use App\Repository\UserRepository;
use App\Controller\User\Helpers\UserHelper;
use App\Controller\UserInvitation\Find\FindInvitationController;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\CurrentUser;
use Symfony\Component\Uid\Uuid;

#[Route("/api/v1/invite")]
class DeclineInvitationController extends AbstractController
{
    public function __construct(
        private readonly UserRepository  $userRepository,
        private readonly EntityManagerInterface   $entityManager,
        private readonly UserHelper               $userHelper,
        private readonly FindInvitationController $findInvitationController
    ) {
    }


    /**
     *
     * @param Uuid $currentId
     * @param Uuid $senderId
     * @param $user
     * @return JsonResponse
     */
    #[Route('/{currentId}/decline/{senderId}', name: 'api_invite_user_decline', defaults: ['inviteeResponse' => false], methods:["POST"])]
    public function declineInvitationResponse(Uuid $currentId, Uuid $senderId, #[CurrentUser] $user): JsonResponse
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $invitee       = $this->userHelper->userExists($this->userRepository, $currentId);
        $currentUserId = $this->userHelper->getCurrentUserId($user);
        if ($currentUserId !== $currentId) {
            return $this->json(
                ['message' => 'Not allowed to pursue current process !'],
                Response::HTTP_NOT_FOUND,
            );
        }

        $inviteeInvitationsObj = $this->findInvitationController->findInvitations($invitee);
        if ($inviteeInvitationsObj instanceof JsonResponse) {
            return $inviteeInvitationsObj;
        }

        $pendingInvitations = $inviteeInvitationsObj->getPending();
        if (!in_array($senderId, $pendingInvitations, true)) {
            return $this->json(
                ['message' => 'No pending invitation exists for theses users'],
                Response::HTTP_NOT_FOUND,
            );
        }

        // Removing declined invitation from pending column
        unset($pendingInvitations[array_search($senderId, $pendingInvitations)]);

        $inviteeInvitationsObj->setPending($pendingInvitations);

        $declinedInvitations = $inviteeInvitationsObj->getDeclined();
        if (in_array($senderId, $declinedInvitations, true)) {
            return $this->json(
                ['message' => 'Should not happen ! Invitation has already been declined.'],
                Response::HTTP_FORBIDDEN,
            );
        }

        $inviteeInvitationsObj->setDeclined([$senderId]);

        $this->entityManager->persist($inviteeInvitationsObj);
        $this->entityManager->flush();
        return $this->json(
            ['message' => 'Invitation has been declined, and is no longer pending'],
            Response::HTTP_OK
        );
    }
}
