<?php

namespace App\Controller\UserInvitation\Create;

use App\Repository\UserRepository;
use App\Controller\User\Helpers\UserHelper;
use App\Entity\UserInvitation;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\CurrentUser;
use Symfony\Component\Uid\Uuid;

#[Route("/api/v1/invite")]
class CreateInvitationController extends AbstractController
{
    public function __construct(
        private readonly UserRepository         $userRepository,
        private readonly EntityManagerInterface $entityManager,
        private readonly UserHelper             $userHelper
    ) {
    }


    /**
     * Pending invitation is set when sender hit the invite button
     * invitee automatically get a pending invitation
     *
     * @param $user
     * @param Uuid $inviteeId
     * @return JsonResponse
     */
    #[Route('/{inviteeId}', name: 'api_invite_user', methods:["POST"])]
    public function createPendingInvitation(
        #[CurrentUser] $user,
        Uuid $inviteeId
    ): JsonResponse {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $senderId = $this->userHelper->getCurrentUserId($user);
        $invitee = $this->userHelper->userExists($this->userRepository, $inviteeId);

        /**
         * Get invitee invitations
         * If user invitations is empty, send new invitation
         */
        $userInvitations    = $invitee->getUserInvitations()->getValues();
        $inviteeInvitationsObj = array_shift($userInvitations);

        if (empty($inviteeInvitationsObj)) {
            $newInvitations = new UserInvitation();
            $newInvitations->setUserId($invitee);
            $newInvitations->setPending(
                [ $senderId ]
            );

            $this->entityManager->persist($newInvitations);
            $this->entityManager->flush();
            return $this->json(
                ['message' => 'Invitation has been sent, and is now pending'],
                Response::HTTP_CREATED,
            );
        }

        // If invitation already exists, check if pending invitation already exists
        $pendingInvitations = $inviteeInvitationsObj->getPending();
        if (!empty($pendingInvitations) && in_array($inviteeId, $pendingInvitations, true)) {
            return $this->json(
                ['message' => 'Should not happen pending invitation already exists'],
                Response::HTTP_FORBIDDEN,
            );
        }

        $inviteeInvitationsObj->setPending(
            [ $inviteeId ]
        );

        $this->entityManager->persist($inviteeInvitationsObj);
        $this->entityManager->flush();
        return $this->json(
            ['message' => 'Invitation has been sent, and is now pending'],
            Response::HTTP_CREATED
        );
    }
}
