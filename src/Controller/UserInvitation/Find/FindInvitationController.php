<?php

namespace App\Controller\UserInvitation\Find;

use App\Entity\User;
use App\Entity\UserInvitation;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class FindInvitationController extends AbstractController
{
    public function __construct(
    ) {
    }

    /**
     * Find user invitations
     *
     * @param  User $user
     * @return JsonResponse|UserInvitation
     */
    public function findInvitations(User $user): JsonResponse|UserInvitation
    {
        $userInvitations    = $user->getUserInvitations()->getValues();
        $userInvitationsObj = array_shift($userInvitations);
        if (empty($userInvitationsObj)) {
            return $this->json(
                ['message' => 'Should not happen invitation must exists'],
                Response::HTTP_FOUND
            );
        }

        return $userInvitationsObj;
    }
}
