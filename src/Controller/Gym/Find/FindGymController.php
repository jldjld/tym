<?php

namespace App\Controller\Gym\Find;

use App\Repository\Interface\GymRepositoryInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Controller class for finding gyms.
 *
 * @package App\Controller\Gym\Find
 * @category Controller
 * @license  https://opensource.org/licenses/MIT MIT License
 * @link     https://example.com
 * @author   Jld
 */
#[Route("/api/v1/gyms")]
class FindGymController extends AbstractController
{
    public function __construct(
        private readonly GymRepositoryInterface $gymRepositoryInterface
    ) {
    }

    /**
     * List gyms by city
     *
     * @param string $city
     * @return JsonResponse
     */
    #[Route('/location/{city}', name: 'api_get_gyms_by_city', methods:["GET"])]
    public function findGymsByCity(string $city): JsonResponse
    {
        // Verify if numbers or special characters exist in $city
        if (preg_match('/[0-9\W]/', $city)) {
            return $this->json(
                ['message' => 'Invalid city name'],
                JsonResponse::HTTP_BAD_REQUEST
            );
        }

        $city       = strtolower($city);
        $foundGyms  = $this->gymRepositoryInterface->findByCity($city);
        if (empty($foundGyms) || !is_array($foundGyms)) {
            return $this->json(
                ['message' => 'No gyms found for this city'],
                JsonResponse::HTTP_NOT_FOUND
            );
        }

        return $this->json(
            $foundGyms,
            JsonResponse::HTTP_OK,
            [],
            ['groups' => ['gym', 'club']]
        );
    }
}
