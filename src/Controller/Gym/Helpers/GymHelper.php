<?php

namespace App\Controller\Gym\Helpers;

use App\Entity\Club;

/**
 * Helper class for Gym
 *
 * @package App\Controller\Gym\Helpers
 * @category Helper
 * @license  https://opensource.org/licenses/MIT MIT License
 * @link     https://example.com
 */
class GymHelper
{
    /**
     * Get Club names from a list of gyms
     *
     * @param  Gym[] $gyms
     * @return Club[]|array
     */
    public static function getGymClubNames(array $gyms): array
    {
        $clubs = [];
        foreach ($gyms as $gym) {
            $club = $gym->getClub();
            if (empty($club)) {
                continue;
            }

            $clubs[] = $club;
        }

        return $clubs;
    }
}
