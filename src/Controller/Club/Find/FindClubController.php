<?php

namespace App\Controller\Club\Find;

use App\Entity\Club;
use App\Repository\Interface\ClubRepositoryInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * Controller class for finding clubs.
 *
 * This class handles the logic for searching and retrieving club information.
 * It provides methods to search for clubs based on various criteria and retrieve
 * detailed information about a specific club.
 *
 * @package App\Controller\Club\Find
 * @category Controller
 * @license  https://opensource.org/licenses/MIT MIT License
 * @link     https://example.com
 * @author   Jld
 */
class FindClubController extends AbstractController
{
    public function __construct(
        private readonly ClubRepositoryInterface $clubRepositoryInterface
    ) {
    }

    /**
     * @return Club[]
     */
    public function findAll(): array
    {
        return $this->clubRepositoryInterface->findAll();
    }
}
