<?php

namespace App\Service\UserMatcher;

use App\Availability\Availability;
use App\Entity\User;
use App\Entity\Gym;
use DateTimeInterface;
use Doctrine\Common\Collections\Collection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class UserMatcher extends AbstractController
{
    private int $_ageDifference = 5;

    public function __construct()
    {
    }


    /**
     * Get random matched gym members
     *
     * @param  User                     $user
     * @param  DateTimeInterface        $birthDate
     * @param  string[]                 $goals
     * @param  Collection<Availability> $availability
     * @return array|User[]
     */
    public function getMatchedMembers(User $user, DateTimeInterface $birthDate, array $goals, Collection $availability): array
    {
        $gym = $user->getMainGym();
        if (empty($gym) || !$gym instanceof Gym) {
            return [];
        }
        // Get Members with verified email only
        $verifiedMembers = $gym->getVerifiedGymMembers();
        /**
         * Get city club members if no gym members exists
         */
        if (!is_array($verifiedMembers)) {
            $club               = $user->getClub();
            $clubId             = $club->getId();
            if (empty($clubId)) {
                return [];
            }

            $verifiedMembers = $club->getVerifiedClubMembers();
        }

        // Randomize members list
        shuffle($verifiedMembers);
        $matchedMembers = $this->sortMatchedMembers($user, $verifiedMembers, $availability, $goals, $birthDate);
        if(empty($matchedMembers)) {
            return [];
        }

        return $matchedMembers;
    }

    /**
     * Sort matched members
     *
     * @param  User                     $user
     * @param  User[]                   $members
     * @param  Collection<Availability> $availability
     * @param  string[]                 $goals
     * @param  DateTimeInterface        $birthDate
     * @return array|User[]
     */
    public function sortMatchedMembers(User $user, array $members, Collection $availability, array $goals, DateTimeInterface $birthDate): array
    {
        $userGender      = $user->getGender();
        $matchedMembers  = [];
        foreach ($members as  $member) {
            // Avoid getting current user in matched results
            if ($user === $member) {
                continue;
            }

            if ($userGender !== $member->getGender()) {
                continue;
            }

            // Make sure both members are available at the same time
            if ($availability !== $member->getAvailabilities()) {
                continue;
            }

            $commonGoals = array_intersect($goals, $member->getGoals());
            if (empty($commonGoals)) {
                continue;
            }
            // Avoid matching users with opposite goals on bulk and cut
            if (in_array('cut', $commonGoals, true) && in_array('bulk', $commonGoals, true)) {
                continue;
            }

            // Avoid matching users with more than 5 years of difference
            $diff = date_diff($birthDate, $member->getBirthdate());
            if ($this->_ageDifference < $diff->y) {
                continue;
            }

            $matchedMembers[] = $member;
        }

        return $matchedMembers;
    }
}
