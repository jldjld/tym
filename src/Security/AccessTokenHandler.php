<?php

namespace App\Security;

use ApiPlatform\Api\UrlGeneratorInterface;
use App\Entity\User;
use App\Repository\Interface\UserRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\ExpiredTokenException;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\InvalidPayloadException;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\InvalidTokenException;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\JWTDecodeFailureException;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\MissingTokenException;
use Lexik\Bundle\JWTAuthenticationBundle\Security\Authenticator\JWTAuthenticator;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\TokenExtractor\TokenExtractorInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\Authenticator\Passport\SelfValidatingPassport;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Exception\UserNotFoundException as ExceptionUserNotFoundException;

class AccessTokenHandler extends JWTAuthenticator
{
    /**
     *
     * @param JWTTokenManagerInterface $jwtManager
     * @param EventDispatcherInterface $eventDispatcher
     * @param TokenExtractorInterface $tokenExtractor
     * @param UserProviderInterface $userProvider
     * @param UrlGeneratorInterface $urlGenerator
     * @param Security $security
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        private JWTTokenManagerInterface $jwtManager,
        private EventDispatcherInterface $eventDispatcher,
        private TokenExtractorInterface $tokenExtractor,
        private UserProviderInterface $userProvider,
        private UserRepositoryInterface $userRepositoryInterface,
        private UrlGeneratorInterface $urlGenerator,
        private Security $security,
        private EntityManagerInterface $entityManager
    ) {
        parent::__construct(
            $this->jwtManager,
            $this->eventDispatcher,
            $this->tokenExtractor,
            $this->userProvider
        );
    }

    /**
     * Called on every request to decide if this authenticator should be
     * used for the request. Returning `false` will cause this authenticator
     * to be skipped.
     */
    public function supports(Request $request): ?bool
    {
        return false !== $this->getTokenExtractor()->extract($request) ? true : throw new MissingTokenException('JWT Token not found');
    }


    /**
     * Handle Api Auth Token
     *
     * @param  Request $request
     * @return Passport
     */
    public function doAuthenticate(Request $request): Passport
    {
        $token   = $this->getTokenExtractor()->extract($request);
        $payload = $this->jwtManager->parse($token);

        try {
            if (!$payload) {
                throw new InvalidTokenException('Invalid JWT Token');
            }
        } catch (JWTDecodeFailureException $e) {
            if (JWTDecodeFailureException::EXPIRED_TOKEN === $e->getReason()) {
                throw new ExpiredTokenException();
            }

            throw new InvalidTokenException('Invalid JWT Token', 0, $e);
        }

        $idClaim = $this->jwtManager->getUserIdClaim();

        if (!isset($payload[$idClaim])) {
            throw new InvalidPayloadException($idClaim);
        }

        /**
         * Must return a UserInterface object
         * (otherwise a UserNotFoundException is thrown)
         */
        $userBadge = new UserBadge(
            (string)$payload[$idClaim],
            function (string $userIdentifier) {
                return $this->userRepositoryInterface->findOneBy(['email' => $userIdentifier]);
            }
        );

        $passport = new SelfValidatingPassport($userBadge);

        $passport->setAttribute('payload', $payload);
        $passport->setAttribute('token', $token);

        return $passport;
    }

    /**
     *
     * @param Request $request
     * @param TokenInterface $token
     * @param string $firewallName
     * @return RedirectResponse|null
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?RedirectResponse // Adjust the return type to Response
    {
        // Get user from request parameter
        $user = $this->security->getUser();
        if (empty($user) || !$user instanceof User) {
            throw new ExceptionUserNotFoundException('Should not happen, user does not exist');
        }

        if ($request->attributes->get('_route') !== 'api_profile' || !$user->isFirstLogin()) {
            return null;
        }

        // Update user first login status
        $user->setIsFirstLogin(false);
        $this->entityManager->flush();

        // Redirect to a route where the user can provide additional data
        try {
            $url = $this->urlGenerator->generate('api_profile_update');
            return new RedirectResponse($url);
        } catch (\Exception $e) {
            throw new \RuntimeException('Could not generate redirect url');
        }
    }
}
