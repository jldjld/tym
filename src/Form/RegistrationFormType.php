<?php

namespace App\Form;

use App\Controller\Gym\Helpers\GymHelper;
use App\Entity\Availability;
use App\Entity\Club;
use App\Entity\User;
use App\Entity\Gym;
use App\Repository\Interface\GymRepositoryInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SearchType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class RegistrationFormType for user registration.
 * @package App\Form
 */
class RegistrationFormType extends AbstractType
{
    public function __construct(
        private readonly GymRepositoryInterface $gymRepositoryInterface,
        private readonly TranslatorInterface $translator,
        private readonly GymHelper $gymHelper
        )
    {
    }

    /**
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $route = $options['label'];
        $user  = $options['data'];
        $city  = $user ? $user->getCity() : null;
        if ($route == 'route_api_profile_update' && $city) {
            $this->updateMandatoryFields( $builder->getForm(),(string) $city);
            return;
        }

        $builder
            ->add(
                'city',
                SearchType::class,
                [
                'required' => false,
                'help'     => $this->translator->trans('Entrez les 3 premieres lettres de votre ville'),
                'label'    => false,
                'constraints'  => new NotNull(), // possible to use an array with more constraints
                'attr'     =>  [
                    'list'         => 'json-cities',
                    'autocomplete' => 'off',
                    'placeholder'  => $this->translator->trans('Rechercher une ville...')
                ]
                ]
            )

            ->add(
                'save',
                SubmitType::class,
                [
                'label' => $this->translator->trans('Trouver ma salle de sport')
                ]
            );

        $builder->get('city')->addEventListener(
            FormEvents::PRE_SUBMIT,
            function (FormEvent $event) {
                $form = $event->getForm();
                $this->registerUser($form->getParent());
            }
        );
    }

    /**
     * Update mandatory fields for user profile.
     * 
     * @param FormInterface $form
     * @param string $city
     * @return FormInterface|null
     */
    public function updateMandatoryFields(FormInterface $form, string $city) : ?FormInterface {
        $gyms           = $this->gymRepositoryInterface->findByCity($city);
        $availabilities = new Availability();

        if (empty($gyms)) {
            return $form->get('city')->addError(
                new FormError(
                    $this->translator->trans('Pas encore de salle partenaire dans votre région :)')
                ),
            );
        }

        $clubs = $this->gymHelper->getGymClubNames($gyms);
        if (! empty($clubs)) {
            $form->add(
                'club',
                EntityType::class,
                [
                'class'        => Club::class,
                'placeholder'  => $this->translator->trans('Sélectionner un club'),
                'choice_label' => 'name',
                'choices'      => $clubs,
                ]
            );
        }

        $form->add(
            'mainGym',
            EntityType::class,
            [
            'row_attr'      => [
                'class' => 'register-city',
                'id'    => 'registration_form_gyms'
            ],
            'required'      => false,
            'class'         => Gym::class,
            'choices'       => $gyms,
            'choice_label'  => 'name',
            'placeholder'   => $this->translator->trans('Sélectionner une salle de sport'),
            ]
        );
        
        $form->add(
            'birthdate',
            BirthdayType::class,
            [
            'label'       => $this->translator->trans('Date de naissance'),
            // TODO Correct datePicker implementation
            // 'html5'       => false,
            'attr'        => ['class' => 'js-datepicker'
            ],
            // 'format' => 'dd-MM-yyyy',
            'widget'      => 'single_text',
            'required'    => false,
            ]
        );
        $form->add(
            'gender',
            ChoiceType::class,
            [
            'required' => false,
            'label'    => $this->translator->trans('Genre'),
            'multiple' => false,
            'expanded' => true,
            'choices'    => [
                'H' => 'M',
                'F' => 'F'
            ]
            ]
        );
        $form->add(
            'goals',
            ChoiceType::class,
            [
            'label'      => $this->translator->trans('Objectifs'),
            'expanded'   => true,
            'multiple'   => true,
            'required'   => false,
            'choices'    => [
                'Prise de masse'          => 'bulk',
                'Perte de poids'          => 'cut',
                'Améliorer son cardio'    => 'cardio',
                'Renforcement musculaire' => 'health',
                'Programme force'         => 'strenght'
            ]
            ]
        );
        
        $form->add(
            'availabilities',
            CollectionType::class,
            [
                'label'         => $this->translator->trans('Disponibilité : Jours'),
                'entry_type'    => CollectionType::class,
                'entry_options' => [
                    'label' => false,
                    'class' => Availability::class,
                    'entry_type' => ChoiceType::class,
                    'entry_options' => [
                        'choices' => [
                            'days' => $availabilities->getDays(),
                            'timeSlot' => $availabilities->getTimeSlot(),
                        ],
                    ],
                ],
            ]
        );

        $form->add(
            'submit',
            SubmitType::class,
            [
            'label' => $this->translator->trans("Mettre à jour"),
            ]
        );

        return $form;
    }

    /**
     * @param FormInterface $form
     * @return void
     */
    private function registerUser(FormInterface $form) : void
    {   
        $form->add(
            'email',
            EmailType::class,
            [
                'required' => false,
            ]
        );

        $form->add(
            'username',
            TextType::class,
            [
                'required'    => false,
                'label'       => $this->translator->trans("Nom d'utilisateur")
            ]
        );


        $confirmPasswordMessage = $this->translator->trans('Confirmer le mot de passe');
        $form->add(
            'plainPassword',
            RepeatedType::class,
            [
            'type'            => PasswordType::class,
            'invalid_message' => $this->translator->trans('Le mot de passe doit être identique pour être enregistré :)'),
            'options'         => ['attr' => [
                'class' => 'password-field',
                ]
            ],
            'required'        => true,
            'first_options'   => [
                'label' => $this->translator->trans('Mot de passe'),
                'help'  => $this->translator->trans('Contient au moins 6 caractères et un chiffre'),
            ],
            'second_options'  => ['label' => $confirmPasswordMessage],
            // instead of being set onto the object directly,
            // this is read and encoded in the controller
            'mapped'      => false,
            'attr'        => [
                'autocomplete' => 'new-password',

            ],
            // Adding password constraints
            'constraints' => [
                new Regex(
                    [
                    'pattern'    => '/\d/',
                    'message'    => $this->translator->trans('Pour la sécurité de votre compte, le mot de passe doit contenir un chiffre'),
                    ]
                ),
                new Length(
                    [
                    'min'        => 6,
                    'minMessage' => $this->translator->trans('Votre mot de passe doit contenir plus de six caractères'),
                    // max length allowed by Symfony for security reasons
                    'max'        => 4096,
                    ]
                ),
            ],
            ]
        );

        $form->add(
            'submit',
            SubmitType::class,
            [
            'label' => $this->translator->trans("S'inscrire"),
            ]
        );

    }

    /**
     *
     * @param  OptionsResolver $resolver
     * @return void
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            [
            'data_class' => User::class,

            // For debugging purpose only
            'csrf_protection' => false,
            ]
        );
    }
}
