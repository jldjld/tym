<?php

namespace App\Entity;

use ApiPlatform\Action\NotFoundAction;
use App\Repository\ClubRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\GetCollection;
use Symfony\Bridge\Doctrine\Types\UuidType;
use Symfony\Component\Uid\Uuid;

#[ORM\Entity(repositoryClass: ClubRepository::class)]
#[ApiResource(
    operations: [
        new Get(security: "is_granted('ROLE_USER')"),
        new Post(
            controller: NotFoundAction::class,
            openapi: false,
            output: false,
            read: false
        ),
        new Delete(
            controller: NotFoundAction::class,
            openapi: false,
            output: false,
            read: false
        ),
        new Patch(
            controller: NotFoundAction::class,
            openapi: false,
            output: false,
            read: false
        ),
        new Put(
            controller: NotFoundAction::class,
            openapi: false,
            output: false,
            read: false
        ),
        new GetCollection()
    ],
    schemes: ['https'],
    normalizationContext: ['groups' => ['gym', 'club']],
    security: "is_granted('ROLE_USER')"
)]
class Club
{
    #[ORM\Id]
    #[ORM\Column(type: UuidType::NAME, unique:true)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: 'doctrine.uuid_generator')]
    #[Groups("club")]
    private ?Uuid $id = null;

    #[ORM\Column(length: 255)]
    #[Groups("club")]
    private ?string $name = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $description = null;

    #[ORM\OneToMany(mappedBy: 'club', targetEntity: User::class)]
    #[Groups("club")]
    private Collection $members;

    #[ORM\OneToMany(mappedBy: 'club', targetEntity: Gym::class)]
    #[Groups("club")]
    private Collection $gyms;

    public function __construct()
    {
        $this->members = new ArrayCollection();
        $this->gyms = new ArrayCollection();
    }

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getMembers(): Collection
    {
        return $this->members;
    }

    /**
     * @return array []|User[]
     */
    public function getVerifiedClubMembers(): array
    {
        $members         = $this->members;
        $verifiedMembers = [];

        foreach ($members as $member) {
            if (true !== $member->isVerified()) {
                continue;
            }

            if (!in_array('ROLE_USER', $member->getRoles())) {
                continue;
            }

            $verifiedMembers[] = $member;
        }

        return $verifiedMembers;
    }

    public function addMember(User $member): self
    {
        if (!$this->members->contains($member)) {
            $this->members->add($member);
            $member->setClub($this);
        }

        return $this;
    }

    public function removeMember(User $member): self
    {
        if ($this->members->removeElement($member)) {
            // set the owning side to null (unless already changed)
            if ($member->getClub() === $this) {
                $member->setClub(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Gym>
     */
    public function getGyms(): Collection
    {
        return $this->gyms;
    }

    public function addGym(Gym $gym): self
    {
        if (!$this->gyms->contains($gym)) {
            $this->gyms->add($gym);
            $gym->setClub($this);
        }

        return $this;
    }

    public function removeGym(Gym $gym): self
    {
        if ($this->gyms->removeElement($gym)) {
            // set the owning side to null (unless already changed)
            if ($gym->getClub() === $this) {
                $gym->setClub(null);
            }
        }

        return $this;
    }
}
