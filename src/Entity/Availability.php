<?php

namespace App\Entity;

use App\Repository\AvailabilityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Types\UuidType;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\Uuid;

#[ORM\Entity(repositoryClass: AvailabilityRepository::class)]
class Availability
{
    private const DEFAULT_WEEK     = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
    private const DEFAULT_TIMESLOT = ['EARLY_MORNING', 'MORNING', 'LUNCH', 'AFTERNOON', 'LATE_AFTERNOON', 'EVENING'];

    #[ORM\Id]
    #[ORM\Column(type: UuidType::NAME, unique:true)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: 'doctrine.uuid_generator')]
    #[Groups("availability:read")]
    private ?Uuid $id = null;

    #[ORM\Column(type: Types::JSON, nullable: true)]
    private ?array $days = null;

    #[ORM\Column(length: 255)]
    private ?array $timeSlot = null;

    #[ORM\ManyToMany(targetEntity: User::class, inversedBy: 'availabilities')]
    private ?Collection $user = null;

    #[ORM\ManyToMany(targetEntity: Gym::class, inversedBy: 'availabilities')]
    private ?Collection $gym = null;

    public function __construct()
    {
        $this->user = new ArrayCollection();
        $this->gym = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(Uuid $id): static
    {
        $this->id = $id;

        return $this;
    }

    public function getDays(): ?array
    {
        return $this->days ?? self::DEFAULT_WEEK;
    }

    public function setDays(?array $days): static
    {
        $this->days = $days;

        return $this;
    }

    public function getTimeSlot(): ?array
    {
        return $this->timeSlot ?? self::DEFAULT_TIMESLOT;
    }

    public function setTimeSlot(string $timeSlot): static
    {
        $this->timeSlot = $timeSlot;

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getUser(): ?Collection
    {
        return $this->user;
    }

    public function addUser(User $user): static
    {
        if (!$this->user->contains($user)) {
            $this->user->add($user);
        }

        return $this;
    }

    public function removeUser(User $user): static
    {
        $this->user->removeElement($user);

        return $this;
    }

    /**
     * @return Collection<int, Gym>
     */
    public function getGym(): ?Collection
    {
        return $this->gym;
    }

    public function addGym(Gym $gym): static
    {
        if (!$this->gym->contains($gym)) {
            $this->gym->add($gym);
        }

        return $this;
    }

    public function removeGym(Gym $gym): static
    {
        $this->gym->removeElement($gym);

        return $this;
    }
}
