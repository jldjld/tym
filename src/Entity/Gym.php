<?php

namespace App\Entity;

use ApiPlatform\Action\NotFoundAction;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Put;
use ApiPlatform\OpenApi\Model;
use ApiPlatform\OpenApi\Model\Parameter;
use App\Controller\Gym\Find\FindGymController;
use App\Repository\GymRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Types\UuidType;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\Uuid;

#[ORM\Entity(repositoryClass: GymRepository::class)]
#[ApiResource(
    operations: [
        new Get(security: "is_granted('ROLE_USER')"),
        new Get(
            routeName: "api_get_gyms_by_city",
            controller: FindGymController::class,
            openapi: new Model\Operation(
                summary: 'Find Gyms by city',
                parameters: [
                    // disable default mandatory identifier parameter
                    new Parameter('id', 'path', 'The identifier of the gym', false),
                    new Parameter('city', 'path', 'The location of searched gyms', true),
                ],
                requestBody: new Model\RequestBody(
                    content:null,
                ),
            ),
            name: 'city',
        ),
        new Post(
            controller: NotFoundAction::class,
            openapi: false,
            output: false,
            read: false
        ),
        new Delete(
            controller: NotFoundAction::class,
            openapi: false,
            output: false,
            read: false
        ),
        new Patch(
            controller: NotFoundAction::class,
            openapi: false,
            output: false,
            read: false
        ),
        new Put(
            controller: NotFoundAction::class,
            openapi: false,
            output: false,
            read: false
        ),
        new GetCollection()
    ],
    schemes: ['https'],
    normalizationContext: ['groups' => ['gym', 'club']],
    security: "is_granted('ROLE_USER')"
)]
class Gym
{
    #[ORM\Id]
    #[ORM\Column(type: UuidType::NAME, unique:true)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: 'doctrine.uuid_generator')]
    #[Groups("gym")]
    private ?Uuid $id = null;

    #[ORM\Column(length: 255)]
    #[Groups("gym")]
    private ?string $name = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $description = null;

    #[ORM\Column(length: 20, nullable: true)]
    private ?string $phone = null;

    #[ORM\ManyToOne(inversedBy: 'owned_gyms')]
    private ?User $owner = null;

    #[ORM\Column(length: 255)]
    #[Groups("gym")]
    private ?string $city = null;

    #[ORM\OneToMany(mappedBy: 'mainGym', targetEntity: User::class, fetch: 'EAGER')]
    private Collection $members;

    #[ORM\ManyToOne(inversedBy: 'gyms')]
    private ?Club $club = null;

    #[ORM\ManyToMany(targetEntity: Availability::class, mappedBy: 'gym')]
    private Collection $availabilities;

    public function __construct()
    {
        $this->members = new ArrayCollection();
        $this->availabilities = new ArrayCollection();
    }

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getClub(): ?Club
    {
        return $this->club;
    }

    public function setClub(?Club $club): self
    {
        $this->club = $club;

        return $this;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(?User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getMembers(): Collection
    {
        return $this->members;
    }

    /**
     * @return array|User[]
     */
    public function getVerifiedGymMembers(): array
    {
        $members         = $this->members;
        $verifiedMembers = [];

        foreach ($members as $member) {
            if (true !== $member->isVerified()) {
                continue;
            }

            if (!in_array('ROLE_USER', $member->getRoles())) {
                continue;
            }

            $verifiedMembers[] = $member;
        }

        return $verifiedMembers;
    }

    public function addMember(User $member): self
    {
        if (!$this->members->contains($member)) {
            $this->members->add($member);
            $member->setMainGym($this);
        }

        return $this;
    }

    public function removeMember(User $member): self
    {
        if ($this->members->removeElement($member)) {
            // set the owning side to null (unless already changed)
            if ($member->getMainGym() === $this) {
                $member->setMainGym(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Availability>
     */
    public function getAvailabilities(): Collection
    {
        return $this->availabilities;
    }

    public function addAvailability(Availability $availability): static
    {
        if (!$this->availabilities->contains($availability)) {
            $this->availabilities->add($availability);
            $availability->addGym($this);
        }

        return $this;
    }

    public function removeAvailability(Availability $availability): static
    {
        if ($this->availabilities->removeElement($availability)) {
            $availability->removeGym($this);
        }

        return $this;
    }
}
