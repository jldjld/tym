<?php

namespace App\Entity;

use App\Repository\UserInvitationRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Action\NotFoundAction;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Put;
use ApiPlatform\OpenApi\Model;
use ApiPlatform\OpenApi\Model\Parameter;
use App\Controller\UserInvitation\Decline\DeclineInvitationController;
use App\Controller\UserInvitation\Accept\AcceptInvitationController;
use App\Controller\UserInvitation\Create\CreateInvitationController;
use ArrayObject;
use Symfony\Bridge\Doctrine\Types\UuidType;
use Symfony\Component\Uid\Uuid;

#[ORM\Entity(repositoryClass: UserInvitationRepository::class)]
#[ApiResource(
    operations: [
        new Get(
            controller: NotFoundAction::class,
            openapi: false,
            output: false,
            read: false
        ),
        new Post(
            routeName: "api_invite_user",
            controller: CreateInvitationController::class,
            openapi: new Model\Operation(
                parameters: [
                    new Parameter('inviteeId', 'path', 'The invitee Id (Invitee)', true),
                ],
                requestBody: new Model\RequestBody(
                    content:null,
                ),
            ),
            denormalizationContext: ['groups' => ['invitation:userId','invitation:accepted', 'invitation:declined', 'invitation:pending']],
            name: 'invitations',
        ),
        new Post(
            routeName: "api_invite_user_accept",
            controller: AcceptInvitationController::class,
            openapi: new Model\Operation(
                responses: [
                    200 => new Model\Response(
                        'Returns a successfull message',
                        new ArrayObject(
                            [
                                'application/json' => [
                                    'message' => 'Invitation has been accepted, and is no longer pending',
                                                                    ],
                            ]
                        ),
                    ),
                ],
                summary: 'Accept a user invitation.',
                description: 'Retrieve matched pending invitation and set it to accepted.',
                parameters: [
                    new Parameter('currentId', 'path', 'Current Logged in user Id', true),
                    new Parameter('senderId', 'path', 'Id of the user who sent invitation', true),
                ],
                requestBody: new Model\RequestBody(
                    content:null,
                )
            ),
            denormalizationContext: ['groups' => ['invitation:userId','invitation:accepted', 'invitation:declined', 'invitation:pending']],
            name: 'accept',
        ),
        new Post(
            routeName: "api_invite_user_decline",
            controller: DeclineInvitationController::class,
            openapi: new Model\Operation(
                responses: [
                    200 => new Model\Response(
                        'Returns a successfull message',
                        new ArrayObject(
                            [
                                'application/json' => [
                                    'message' => 'Invitation has been accepted, and is no longer pending',
                                ],
                            ]
                        ),
                    ),
                ],
                summary: 'Decline a user invitation.',
                description: 'Retrieve matched pending invitation and set it to declined.',
                parameters: [
                    new Parameter('currentId', 'path', 'Current Logged in user Id', true),
                    new Parameter('senderId', 'path', 'Id of the user who sent invitation', true),
                ],
                requestBody: new Model\RequestBody(
                    content:null,
                ),
            ),
            denormalizationContext: ['groups' => ['invitation:userId','invitation:accepted', 'invitation:declined', 'invitation:pending']],
            name: 'decline',
        ),
        new Delete(
            controller: NotFoundAction::class,
            openapi: false,
            output: false,
            read: false
        ),
        new Patch(
            controller: NotFoundAction::class,
            openapi: false,
            output: false,
            read: false
        ),
        new Put(
            controller: NotFoundAction::class,
            openapi: false,
            output: false,
            read: false
        ),
        new GetCollection(
            controller: NotFoundAction::class,
            openapi: false,
            output: false,
            read: false
        )
    ],
    schemes: ['https'],
    normalizationContext: ['groups' => ['invitation:userId','invitation:accepted', 'invitation:declined', 'invitation:pending']],
    security: "is_granted('ROLE_USER')"
)]

class UserInvitation
{
    #[ORM\Id]
    #[ORM\Column(type: UuidType::NAME, unique:true)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: 'doctrine.uuid_generator')]
    #[Groups("invitation:userId")]
    private ?Uuid $id = null;

    #[ORM\Column(type: Types::JSON, nullable: true)]
    private array $accepted = [];

    #[ORM\Column(type: Types::JSON, nullable: true)]
    private array $declined = [];

    #[ORM\ManyToOne(inversedBy: 'userInvitations')]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $user = null;

    #[ORM\Column(type: Types::JSON, nullable: true)]
    private array $pending = [];

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    #[Groups(["invitation:accepted"])]
    public function getAccepted(): array
    {
        return $this->accepted;
    }

    public function setAccepted(?array $accepted): self
    {
        $this->accepted = $accepted;

        return $this;
    }

    #[Groups(["invitation:declined"])]
    public function getDeclined(): array
    {
        return $this->declined;
    }

    public function setDeclined(?array $declined): self
    {
        $this->declined = $declined;

        return $this;
    }

    #[Groups(["invitation:userId"])]
    public function getUserId(): ?User
    {
        return $this->user;
    }

    public function setUserId(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    #[Groups(["invitation:pending"])]
    public function getPending(): array
    {
        return $this->pending;
    }

    public function setPending(?array $pending): self
    {
        $this->pending = $pending;

        return $this;
    }
}
