<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Action\NotFoundAction;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Put;
use ApiPlatform\OpenApi\Model;
use ApiPlatform\OpenApi\Model\Parameter;
use ArrayObject;
use Symfony\Bridge\Doctrine\Types\UuidType;
use Symfony\Component\Uid\Uuid;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\Table(name: '`user`')]
#[UniqueEntity(fields: ['email'], message: 'Cette adresse email est déja associée à un compte')]
#[UniqueEntity(fields: ['email'], message: 'Cette adresse email est déja associée à un compte')]
#[ApiResource(
    operations: [
        new Get(
            routeName: "api_profile",
            openapi: new Model\Operation(
                summary: 'Retrieves logged in user profile data',
                parameters: [
                    // disable default mandatory identifier parameter
                    new Parameter('id', 'path', 'User Id', false, false, true),
                ]
            ),
            security: "is_granted('ROLE_USER')",
            name: 'single user',
        ),
        new Get(
            routeName: "api_profile_id",
            security: "is_granted('ROLE_USER')",
            name: 'single user',
        ),
        new Get(
            routeName: "api_profile_update",
            security: "is_granted('ROLE_USER')",
            name: 'single user',
        ),
        new Post(
            routeName: "api_register",
            openapi: new Model\Operation(
                responses: [
                    201 => new Model\Response(
                        'Returns the user id created',
                        new ArrayObject(
                            [
                                'application/json' => [
                                    'user_id' => 'id of the user created',
                                ],
                            ]
                        ),
                    ),
                ],
                summary: 'Registers a new User',
                requestBody: new Model\RequestBody(
                    content: new \ArrayObject([
                        'application/json' => [
                            'schema' => [
                                'type' => 'object',
                                'properties' => [
                                    'email'    => ['type' => 'email'],
                                    'password' => ['type' => 'array', 'items' => [ 'type' => 'string']],
                                    'city'     => ['type' => 'string'],
                                    'username' => ['type' => 'string'],
                                ]
                            ],
                            'example' => [
                                'email'        => 'toto@example.fr',
                                'plainPassword' => [
                                    'first'  => 'password',
                                    'second' => 'password'],
                                'city'         => 'Nantes',
                                'username'     => 'toto'
                            ]
                        ]
                    ])
                )
            ),
            denormalizationContext: ['groups' => 'write:user'],
            name: 'register'
        ),
        new Post(
            uriTemplate: '/login',
            openapi: new Model\Operation(
                summary: 'Authenticates a User',
            ),
            denormalizationContext: ['groups' => "auth:user"],
            name: 'login'
        ),
        new Delete(
            security: "is_granted('ROLE_ADMIN')"
        ),
        new Patch(
            routeName: "api_profile_update",
            openapi: new Model\Operation(
                summary: 'Updates logged in user data',
                parameters: [
                    // disable default mandatory identifier parameter
                    new Parameter('id', 'path', 'User Id', false),
                ],
                requestBody: new Model\RequestBody(
                    content: new \ArrayObject([
                        'application/json' => [
                            'schema' => [
                                'type' => 'object',
                                'properties' => [
                                    'birthDate'    => ['type' => 'dateTime'],
                                    'goals'        => ['type' => 'array'],
                                    'gender'       => ['type' => 'string'],
                                    'availability' => ['type' => 'array', 'items' => ['type' => Availability::class]],
                                ]
                            ],
                            'example' => [
                                'birthdate'    => '01/01/2000',
                                'goals'        => ['lose weight', 'gain muscle'],
                                'gender'       => 'F',
                                'availability' => new Availability()
                            ]
                        ]
                    ])
                )
            ),
            description: 'Updates user data on first login',
            security: "is_granted('ROLE_USER')",
            name: 'Update user data',
        ),
        new Put(
            controller: NotFoundAction::class,
            openapi: false,
            output: false,
            read: false
        ),
        new GetCollection(
            security: "is_granted('ROLE_ADMIN')"
        )
    ],
    schemes: ['https'],
    normalizationContext: ['groups' => ['user', 'gym', 'club']]
)]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\Column(type: UuidType::NAME, unique:true)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: 'doctrine.uuid_generator')]
    #[Groups("user")]
    private ?Uuid $id = null;

    #[ORM\Column(length: 180, unique: true)]
    #[Groups(["user","write:user","auth:user"])]
    private ?string $email = null;

    #[ORM\Column]
    private array $roles = [];

    /**
     * @var string The hashed password
     */
    #[ORM\Column]
    #[Groups(["write:user", "auth:user"])]
    private ?string $password = null;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    #[Groups(["user","write:user"])]
    private ?\DateTimeInterface $birthdate = null;

    #[ORM\Column(length: 1, nullable: true)]
    #[Groups(["user","write:user"])]
    private ?string $gender = null;

    #[ORM\Column(type: Types::JSON)]
    #[Groups(["user","write:user"])]
    private array $goals = [];

    #[ORM\Column(length: 255)]
    #[Groups(["user","write:user"])]
    private ?string $city = null;

    #[ORM\OneToMany(mappedBy: 'owner', targetEntity: Gym::class)]
    private ?Collection $owned_gyms;

    #[ORM\Column(length: 55)]
    #[Groups(["user","write:user"])]
    private ?string $username = null;

    #[ORM\Column(type: Types::BOOLEAN)]
    private ?bool $isVerified = false;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(["user","write:user"])]
    private ?string $avatar = null;

    #[ORM\ManyToOne(fetch: 'EAGER', inversedBy: 'members')]
    #[Groups(["user","write:user"])]
    private ?Gym $mainGym = null;

    #[ORM\ManyToOne(inversedBy: 'members')]
    #[Groups(["user","write:user"])]
    private ?Club $club = null;

    #[ORM\Column(type: Types::BOOLEAN)]
    private bool $isFirstLogin = true;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: UserInvitation::class, orphanRemoval: true)]
    private Collection $userInvitations;

    #[ORM\ManyToMany(targetEntity: Availability::class, mappedBy: 'user')]
    private ?Collection $availabilities;

    public function __construct()
    {
        $this->owned_gyms = new ArrayCollection();
        $this->userInvitations = new ArrayCollection();
        $this->availabilities = new ArrayCollection();
    }

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getBirthdate(): ?\DateTimeInterface
    {
        return $this->birthdate;
    }

    public function setBirthdate(?\DateTimeInterface $birthdate): self
    {
        $this->birthdate = $birthdate;

        return $this;
    }

    public function getGender(): ?string
    {
        return $this->gender;
    }

    public function setGender(string $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    public function getGoals(): array
    {
        return $this->goals;
    }

    public function setGoals(array $goals): self
    {
        $this->goals = $goals;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return Collection<int, Gym>
     */
    public function getOwnedGyms(): Collection
    {
        return $this->owned_gyms;
    }

    public function addOwnedGym(Gym $ownedGym): self
    {
        if (!$this->owned_gyms->contains($ownedGym)) {
            $this->owned_gyms->add($ownedGym);
            $ownedGym->setOwner($this);
        }

        return $this;
    }

    public function removeOwnedGym(Gym $ownedGym): self
    {
        if ($this->owned_gyms->removeElement($ownedGym)) {
            // set the owning side to null (unless already changed)
            if ($ownedGym->getOwner() === $this) {
                $ownedGym->setOwner(null);
            }
        }

        return $this;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function isVerified(): ?bool
    {
        return $this->isVerified;
    }

    public function setIsVerified(bool $isVerified): self
    {
        $this->isVerified = $isVerified;

        return $this;
    }

    public function getAvatar(): ?string
    {
        return $this->avatar;
    }

    public function setAvatar(?string $avatar): self
    {
        $this->avatar = $avatar;

        return $this;
    }

    public function getMainGym(): ?Gym
    {
        return $this->mainGym;
    }

    public function setMainGym(?Gym $mainGym): self
    {
        $this->mainGym = $mainGym;

        return $this;
    }

    public function getClub(): ?Club
    {
        return $this->club;
    }

    public function setClub(?Club $club): self
    {
        $this->club = $club;

        return $this;
    }

    public function isFirstLogin(): bool
    {
        return $this->isFirstLogin;
    }

    public function setIsFirstLogin(bool $isFirstLogin): self
    {
        $this->isFirstLogin = $isFirstLogin;

        return $this;
    }

    /**
     * @return Collection<int, UserInvitation>
     */
    public function getUserInvitations(): Collection
    {
        return $this->userInvitations;
    }

    public function addUserInvitation(UserInvitation $userInvitation): self
    {
        if (!$this->userInvitations->contains($userInvitation)) {
            $this->userInvitations->add($userInvitation);
            $userInvitation->setUserId($this);
        }

        return $this;
    }

    public function removeUserInvitation(UserInvitation $userInvitation): self
    {
        if ($this->userInvitations->removeElement($userInvitation)) {
            // set the owning side to null (unless already changed)
            if ($userInvitation->getUserId() === $this) {
                $userInvitation->setUserId(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|null
     */
    public function getAvailabilities(): ?Collection
    {
        return $this->availabilities;
    }

    public function addAvailability(Availability $availability): static
    {
        if (!$this->availabilities->contains($availability)) {
            $this->availabilities->add($availability);
            $availability->addUser($this);
        }

        return $this;
    }

    public function removeAvailability(Availability $availability): static
    {
        if ($this->availabilities->removeElement($availability)) {
            $availability->removeUser($this);
        }

        return $this;
    }
}
