<?php
/**
 * DataFixtures
 */

namespace App\DataFixtures;

use App\Entity\Club;
use App\Entity\Gym;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

/**
 * Implement fixtures
 */
class AppFixtures extends Fixture
{
    /**
     * Load fixtures
     *
     * @param  ObjectManager $manager
     * @return void
     */
    public function load(ObjectManager $manager): void
    {
        $opening_hours = [
            'days' => [ 'monday', 'tuesday', 'wenesday', 'thursday', 'friday' ],
            'start_time' => '06:00',
            'end_time'   => '19:00'
        ];

        $basic_fit = new Club();
        $basic_fit->setName('Basic Fit')
            ->setDescription('Lorem ipsum');
        $manager->persist($basic_fit);

        $fitness_park = new Club();
        $fitness_park->setName('Fitness Park')
            ->setDescription('Lorem ipsum');
        $manager->persist($fitness_park);

        $gym = new Gym();
        $gym->setName('Basic Fit Mangin')
            ->setClub($basic_fit)
            ->setCity('Nantes')
            ->setOpeningHours($opening_hours)
            ->setPhone('0000000000');
        $manager->persist($gym);

        $gym = new Gym();
        $gym->setName('Basic Fit Bouffay')
            ->setClub($basic_fit)
            ->setCity('Nantes');
        $manager->persist($gym);

        $gym = new Gym();
        $gym->setName('Fitness park Malakoff')
            ->setClub($fitness_park)
            ->setCity('Nantes')
            ->setOpeningHours($opening_hours)
            ->setPhone('0000000001');
        $manager->persist($gym);

        $gym = new Gym();
        $gym->setName('Fitness park Lyon')
            ->setClub($fitness_park)
            ->setCity('Lyon');
        $manager->persist($gym);

        $manager->flush();
    }
}
