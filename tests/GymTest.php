<?php

namespace App\Tests;

use Symfony\Component\HttpFoundation\Response;

/**
 * Testing access to Gyms path
 */
class GymTest extends AbstractTest
{
    /**
     * Test get Gyms without authentication
     *
     * @return void
     */
    public function testGetGymsKo(): void
    {
        $response = static::createClient()->request('GET', '/api/v1/gyms');
        $this->assertResponseHeaderSame('content-type', 'application/json');

        $this->assertJsonContains(['message' => 'JWT Token not found']);
        $this->assertResponseStatusCodeSame(Response::HTTP_UNAUTHORIZED);
    }

    /**
     * Test get Gyms with authentication
     *
     * @return void
     */
    public function testGetGymsOk(): void
    {
        // env variables located in .env.test
        $token = $this->getToken(
            [
            'email'    => getenv('API_USERNAME'),
            'password' => getenv('API_PASSWORD'),]
        );

        $response = $this->createClientWithCredentials($token)->request('GET', '/api/v1/gyms');
        $this->assertResponseStatusCodeSame(200);
    }
}
