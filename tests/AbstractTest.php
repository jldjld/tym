<?php

namespace App\Tests;

use ApiPlatform\Symfony\Bundle\Test\ApiTestCase;
use ApiPlatform\Symfony\Bundle\Test\Client;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Authentication Test class
 */
class AbstractTest extends ApiTestCase
{
    private ?string $_token = null;
    protected ?EntityManagerInterface $em;

    /**
     * Undocumented function
     *
     * @return void
     */
    public function setUp(): void
    {
        $kernel   = self::bootKernel();
        $this->em = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    /**
     * Building authentication header
     *
     * @param  [type] $token
     * @return Client
     */
    protected function createClientWithCredentials($token = null): Client
    {
        $token = $token ?: $this->getToken();
        // env variables located in .env.test
        return static::createClient([], ['headers' => ['authorization' => 'Bearer '.$token], 'base_uri' => getenv('API_BASE_URI')]);
    }

    /**
     * Get token from basic auth
     *
     * @param  [type] $body
     * @return string
     */
    protected function getToken($body = null): string
    {
        if ($this->_token) {
            return $this->_token;
        }

        $response = static::createClient()->request(
            'POST',
            '/api/v1/login',
            [
                'headers' => ['content-type' => 'application/ld+json; charset=utf-8'],
                'json' =>
                    $body
                ,
            ]
        );

        $this->assertResponseIsSuccessful();
        $data = json_decode($response->getContent());

        $this->_token = $data->token;
        return $data->token;
    }
}
