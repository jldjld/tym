<?php

namespace App\Tests;

use Symfony\Component\HttpFoundation\Response;

/**
 * Testing User Invitation process
 */
class UserInvitationTest extends AbstractTest
{

    private int $_inviteeId = 49;
    private int $_pendingInviteeId = 48;
    /**
     * Test create user invitation
     * Don't forget to modify invitee ID !
     *
     * @return void
     */
    public function testCreateInvitationOk(): void
    {
        // env variables located in .env.test
        $token = $this->getToken(
            [
            'email'    => getenv('API_USERNAME'),
            'password' => getenv('API_PASSWORD'),
            ]
        );

        $this->createClientWithCredentials($token)->request('POST', '/api/v1/invite/' . $this->_inviteeId);
        $this->assertJsonContains(['message' => 'Invitation has been sent, and is now pending']);
        $this->assertResponseStatusCodeSame(Response::HTTP_CREATED);

    }

    /**
     * Don't forget to modify pendingInviteeId 
     * with an already pending invitee !
     *
     * @return void
     */
    public function testCreateInvitationKo(): void
    {
        // env variables located in .env.test
        $token = $this->getToken(
            [
            'email'    => getenv('API_USERNAME'),
            'password' => getenv('API_PASSWORD'),
            ]
        );

        $this->createClientWithCredentials($token)->request('POST', '/api/v1/invite/' . $this->_pendingInviteeId);
        $this->assertJsonContains(['message' => 'Should not happen pending invitation already exists']);
        $this->assertResponseStatusCodeSame(Response::HTTP_FORBIDDEN);
    }
}
