<?php

namespace App\Tests;

use Symfony\Component\HttpFoundation\Response;
use App\Entity\User;
use App\Helpers\UserHelper;

/**
 * Testing user auth,
 * registration and access to profiles data
 */
class UserTest extends AbstractTest
{
    private int $_memberId = 32;
    /**
     * Test login returns a token
     *
     * @return void
     */
    public function testLoginTokenOk(): void
    {
        // env variables located in .env.test
        $token = $this->getToken(
            [
            'email'    => getenv('API_USERNAME'),
            'password' => getenv('API_PASSWORD'),]
        );

        $this->assertResponseIsSuccessful();
        $this->assertJsonContains(['token' => $token]);
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function testProfileAccessOk(): void
    {
        /**
         * Get current loggedin user from its email adress
         */
        $currentUser = $this->em->getRepository(User::class)->findOneBy(['email' => getenv('API_USERNAME')]);
        $age         = UserHelper::getMemberAge($currentUser->getBirthDate());

        // env variables located in .env.test
        $token = $this->getToken(
            [
            'email'    => getenv('API_USERNAME'),
            'password' => getenv('API_PASSWORD'),
            ]
        );


        $response = $this->createClientWithCredentials($token)->request('GET', '/api/v1/profile');
        // Verify returned Json response
        $this->assertJsonContains(
            [
                [
                    'username'     =>  $currentUser->getUsername(),
                    'age'          => $age->y,
                    'goals'        => $currentUser->getGoals(),
                    'availability' => $currentUser->getAvailability(),
                    'avatar'       => $currentUser->getAvatar(),],
                    [
                        'is_verified' => $currentUser->isVerified()
                    ],
            ]
        );

        $this->assertResponseStatusCodeSame(Response::HTTP_FOUND);
    }

    /**
     * Get other profiles data
     *
     * @return void
     */
    public function testProfilesAccessOk(): void
    {
        // env variables located in .env.test
        $token = $this->getToken(
            [
            'email'    => getenv('API_USERNAME'),
            'password' => getenv('API_PASSWORD'),]
        );

        // Test access to existing profile member
        $this->createClientWithCredentials($token)->request('GET', '/api/v1/profile/' . $this->_memberId );
        $this->assertResponseStatusCodeSame(Response::HTTP_FOUND);
    }

    /**
     * Get other profiles data without token
     *
     * @return void
     */
    public function testProfilesAccessKo(): void
    {
        // Test access to existing profile member
        $response = static::createClient()->request('GET', '/api/v1/profile/toto');


        $this->assertJsonContains(['message' => 'JWT Token not found']);
        $this->assertResponseStatusCodeSame(Response::HTTP_UNAUTHORIZED);
    }


    /**
     * Register new user
     *
     * @return void
     */
    public function testRegisterUserOk()
    {
        $randomUsername = 'test-registration-' . random_int(0, 100);
        $response = static::createClient()->request(
            'POST',
            '/api/v1/register',
            ['json' => [
                    'plainPassword' => [
                        'first' => 'tototo1',
                        'second' => 'tototo1'
                    ],
                    'username' => $randomUsername,
                    'email'    => $randomUsername . '@test.test',
                    'city' => 'Nantes',
                ]
            ]
        );

        $this->assertResponseStatusCodeSame(Response::HTTP_CREATED);
    }

    /**
     * Register new user with uncorrect field value
     *
     * @return void
     */
    public function testRegisterUserKo()
    {
        $randomUsername = 'test-registration-' . random_int(0, 100);
        $response = static::createClient()->request(
            'POST',
            '/api/v1/register',
            ['json' => [
                    // Password uncorrect lenght and must contain one int
                    'plainPassword' => [
                        'first' => 'toto',
                        'second' => 'toto'
                    ],
                    'username' => $randomUsername,
                    'email'    => $randomUsername . '@test.test',
                ]
            ]
        );

        $this->assertResponseStatusCodeSame(Response::HTTP_BAD_REQUEST);
    }
}
